<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config =  
array(
    // 'site/contact' est la clée du tableau de configuration qui permet de données des règles à la méthode contact
    'site/contact' => array(
        array(
            'field' => 'name',
            'label' => 'Nom',
            'rules' => 'required'
        ),
        array(
            'field' => 'email',
            'label' => 'E-mail',
            'rules' => array('valid_email', 'required')
        ),
        array(
            'field' => 'title',
            'label' => 'Titre',
            'rules' => 'required'
        ),
        array(
            'field' => 'title',
            'label' => 'Titre',
            'rules' => 'required'
        )
    ),

    // Pour la connexion exige simplement le nom d'utilisateur et le mot de passe
    'site/connexion' => array(
        array(
            'field' => 'username',
            'label' => "Nom d'utilisateur",
            'rules' => 'required'
        ), 
        array(
            'field' => 'password',
            'label' => 'Mot de passe',
            'rules' => 'required'
        )
    )    
);