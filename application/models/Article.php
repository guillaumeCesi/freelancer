<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Model
{
    protected $_alias;
    protected $_author;
    protected $_author_id;
    protected $_content;
    protected $_date;
    protected $_id;
    protected $_status;
    protected $_title;

    public function __construct()
    {
        parent::__construct();
        $this->clear_date();
    }

    public function __get($key)
    {
        $method_name = 'get_property_'. $key;
        if(method_exists($this, $method_name))
        {
            
        }
    }
}