<div class="jumbotron text-center">
  <div class="container">
    <h1><?=$title?></h1>
    <p>TEST blog Freelancer avec article ...</p>
  </div>
</div>

<div class="container text-center">
    <div class="containertext-center">
        <div class="col-md-12">
        <p class="text-center">Nombre d'articles : <?= $this->articles->num_items; ?></p>
        </div>
    </div>
    <div class="row">
        <?php if ($this->articles->has_items) : ?>
          <?php
          foreach($this->articles->items as $article) 
          {
              $this->load->view('blog/article_resume', $article);
          }
          ?>
        <?php else: ?>
        <div class="col-md-12">
            <p class="alert alert-warning" role="alert">
            Il n'y a encore aucun article.
            </p>
        </div>
        <?php endif; ?>
  </div>
</div>


