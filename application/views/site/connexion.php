<div class="jumbotron text-center">
  <div class="container">
    <h1><?=$title?></h1>
    <p>Veuillez entrer votre login et mot de passe</p>
  </div>
</div>

<div class="container">
  <div class="row">
  </div>
    <div class="container col-md-2> <!-- permet de centrer un bloc: "col-md-2 ml-auto mr-auto" -->
      <div class="row justify-content-center">
        <?= form_open('connexion', ['class' => 'form-group row']); ?>
        <?php if (!empty($login_error)): ?>
        <div class="form-group form-group-inline">
          <div class="col-md-offset-2 col-md-10 has-error">
            <span class="help-block"><?= $login_error; ?></span>
          </div>
        </div>
        <?php endif; ?>
        <div class="form-group form-inline">
          <?= form_label("Login:", "username", ['class' => "col-md-2 control-label"]) ?>
          <div class="col-md-10 <?= empty(form_error('username')) ? "" : "has-error" ?>">
            <?= form_input(['name' => "username", 'id' => "username", 'class' => 'form-control'], set_value('username')) ?>
            <span class="help-block"><?= form_error('username'); ?></span>
          </div>
        </div>
        <div class="form-group form-inline">
          <?= form_label("Password:", "password", ['class' => "col-md-2 control-label"]) ?>
          <div class="col-md-10 <?= empty(form_error('password')) ? "" : "has-error" ?>">
            <?= form_password(['name' => "password", 'id' => "password", 'class' => 'form-control'], set_value('password')) ?>
            <span class="help-block"><?= form_error('password'); ?></span>
          </div>
        </div>
        <div class="form-group form-inline">
          <div class="col-md-offset-2 col-md-10">
            <?= form_submit("send", "Connexion", ['class' => "btn btn-default"]); ?>
          </div>
        </div>
        <?= form_close() ?>
      </div>
  </div>
</div>  
