
<div class="jumbotron text-center">
  <div class="container">
    <h1><?=$title?></h1>
    <p>Le formulaire du contact pour tout renseignement ...</p>
  </div>
</div>

<div class="container col-md-2"> <!-- permet de centrer un bloc: "col-md-2 ml-auto mr-auto" -->
    <div class="row ">
        <?= form_open('contact', ['class' => 'form-group row']); ?>
        <div class="form-group form-inline" ">
            <?= form_label("Nom:", "name", ['class' => "col-md-2 control-label"]) ?>
                <div class="col-md-10 <?= empty(form_error('name')) ?'':'has-error' ?>">
                    <?= form_input(['name' => "name", 'id' => "name", 'class' => 'form-control'],set_value('name')) ?>
                    <span class="help-block"><?= form_error('name');?></span>
                </div>
        </div>
        <div class="form-group form-inline">
            <?= form_label("Email:", "email", ['class' => "col-md-2 control-label "]) ?>
            <div class="col-md-10 <?= empty(form_error('email')) ?'':'has-error' ?>"> <!-- on ajoute la classe "has-error" au bloc contenant le cchamp du formulaire -->
                <?= form_input(['name' => "email", 'id' => "email", 'type' => 'email', 'class' => 'form-control'], set_value('email')) ?>
                <span class="help-block"><?= form_error('email');?></span>
            </div>
        </div>
        <div class="form-group form-inline">
            <?= form_label("Titre:", "title", ['class' => "col-md-2 control-label "]) ?>
            <div class="col-md-10 <?= empty(form_error('title')) ?'':'has-error' ?>">
                <?= form_input(['name' => "title", 'id' => "title", 'class' => 'form-control'],set_value('title')) ?>
                <span class="help-block"><?= form_error('title');?></span>
            </div>
        </div>
        <div class="form-group">
            <?= form_label("Message:", "message", ['class' => "col-md-2 control-label "]) ?>
            <div class="col-md-10 <?= empty(form_error('message')) ?'':'has-error' ?>">
                <?= form_textarea(['name' => "message", 'id' => "message", 'class' => 'form-control'],set_value('message')) ?>
                <span class="help-block"><?= form_error('message'); ?></span>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <?= form_submit("send", "Envoyer", ['class' => "btn btn-default"]); ?>
            </div>
        </div>
        <?= form_close() ?>
    </div>
</div>
