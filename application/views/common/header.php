<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $title ?></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![ endif ] -->
  </head>
  <body>

<header>
	<!-- Debut Navbar  -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="<?= site_url('index'); ?>">Accueil</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
          <a class="nav-link" href="<?= site_url('ffreelancer'); ?>">Freelancer</a>
          </li>
          <li class="nav-item">
          <a class="nav-link" href="<?= site_url('blog'); ?>">Blog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= site_url('fclient'); ?>">Client</a>
          </li> 
          <?php if ($this->auth_user->is_connected) : ?>
            <li>
              <a class="nav-link" href="<?= site_url('panneau_de_controle/index'); ?>">Panneau de contrôle</a>
            </li>
          <?php endif; ?>
          <li class= "nav-item">
            <a class="nav-link" href="<?= site_url('contact'); ?>">Contact</a>
          </li> 
          <input class="form-control mr-sm-2" type="text" placeholder="Rechercher" aria-label="Rechercher">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher</button>
        </ul>
        <ul class="nav navbar-nav navbar-right">

        <!-- Le contrôle d'accès il permet de savoir si un utilisateur est authentifié -->
        <?php if($this->auth_user->is_connected) : ?>
          <li><?= anchor('deconnexion', "Déconnexion"); ?></li>
        <?php else: ?>
          <li><?= anchor('connexion', "Connexion"); ?></li>
        <?php endif; ?>
        </ul>
        <?php if($this->auth_user->is_connected) : ?>
          <p class="navbar-text navbar-right">|</p>
          <p class="navbar-text navbar-right">Bienvenue <strong><?= $this->auth_user->username; ?></strong></p>
        <?php endif; ?>
      </div>
    </nav><!-- Fin Navbar -->
</header>