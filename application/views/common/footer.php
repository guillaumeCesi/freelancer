<div class="container">
      <hr>
      <footer class="container text-center">
      <p>&copy; Freelancer</p><br/>
      <p>Guillaume Soulichanh 2017-2019</p>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?= base_url('js/jquery-3.3.1.slim.min.js');?>"></script>
    <!--<script src="js/jquery-3.3.1.slim.min.js"></script>-->
    <script src="js/bootstrap.bundle.min.js"></script>
  </body>
</html>
