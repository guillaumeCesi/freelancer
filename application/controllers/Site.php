<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Site extends CI_Controller 
    {
    
        public function index()
        {
            $data["title"] = "Page d'accueil";// Titre de la page

            $this->load->view('common/header', $data); // appel de la librairies Loader pour charger la vue
            $this->load->view('site/index', $data);
            $this->load->view('common/footer', $data);
        }

        public function ffreelancer()
        {
            $data["title"] = "Page freelancer";
            
            $this->load->view('common/header', $data);
            $this->load->view('site/ffreelancer', $data);
            $this->load->view('common/footer', $data);
        }
        
        public function fclient()
        {
            $data["title"] = "Page client";

            $this->load->view('common/header', $data);
            $this->load->view('site/fclient', $data);
            $this->load->view('common/footer', $data);
        }

        public function contact()
        {
            $this->load->helper("form"); // Charge le "Form Helper"
            $this->load->library('form_validation'); // Charge la librairie "form_validation" qui se trouve dans "/config/form_validation.php"

            $data["title"] = "Contact"; // Titre de la page

            $this->load->view('common/header', $data); // appel de la librairies Loader pour charger la vue
            if($this->form_validation->run())
            {
                // envoie du mail

                // on charge d'abord les paramètres pour envoyer le mail
                $this->load->library('email');
                $this->config->load('email', TRUE); // Le paramètre True permet de créer une seule entrée mail
                $this->email->initialize($this->config->item('email'));

                $this->email->from($this->input->post('email'), $this->input->post('name')); // from() défini la source du message
                $this->email->to('g.soulichanh@gmail.com'); // to() Défini le destinataire
                $this->email->subject($this->input->post('title')); // subject() défini le sujet du message
                $this->email->message($this->input->post('message'));

                //$this->email->send();
                if($this->email->send()) 
                { // send() fontion pour envoyer le mail
                    $data['result_class'] = "alert-success";
                    $data['result_message'] = "Merci de nous avoir envoyé ce mail. Nous y répondrons dans les meilleurs délais.";
                } 
                else 
                {
                    $data['result_class'] = "alert-danger";
                    $data['result_message'] = "Votre message n'a pas pu être envoyé. Nous mettons tout en oeuvre pour résoudre le problème.";
                    // Test d'échec envoie de mail
                    $data['result_message'] .= "<pre>\n";
                    $data['result_message'] .= $this->email->print_debugger();
                    $data['result_message'] .= "</pre>\n";
                    $this->email->clear();
                }
                $this->load->view('site/contact_result', $data);
            }
            else
            {
                $this->load->view('site/contact', $data);
            }
            $this->load->view('common/footer', $data);
        }

        public function session_test() 
        {
            $this->session->count ++;
            echo"Valeur :" . $this->session->count;
        }

        public function connexion() 
        {
            $this->load->helper("form");
            $this->load->library('form_validation');
    
            $data["title"] = "Identification";
    
            if($this->form_validation->run()) 
            {
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $this->auth_user->login( $username, $password);
                if($this->auth_user->is_connected) 
                {
                    redirect('index');
                } 
                else 
                {
                    $data['login_error'] = "Échec de l'authentification";
                }
            } 
            else 
            {
                $this->load->view('common/header', $data);
                $this->load->view('site/connexion', $data);
                $this->load->view('common/footer', $data);
            }
        }

        function deconnexion() 
        {
            $this->auth_user->logout();
            redirect('index');
        }
    }
?>